##########################################################
#
# First R program
#
# Antoine Lamer
#
# 22/08/08
#
# Objectives :
# Load two csv files : patient.csv and hospital_stay.csv
# Describe the two files
# Merge patients and hospital stays
# Transform some dates
# Compute new variables
# Explore the dataset
# Display a graphic
#
##########################################################



##########################################################
#
# Variables names :
# - no special character except _
# - lowercase
# - no useless words (e.g., dataset)
# - avoid number to prefer a description of the state of the object
# (e.g. patient_deduplicated)
#
##########################################################


# Path
# Update the path with your own directory
##########################################################
path_data = "C:/Users/Frank/Desktop/git/r_first_program/data/"
# Read the files
patient = read.csv2(file.path(path_data, "patient.csv"))
hospital_stay = read.csv2(file.path(path_data, "hospital_stay.csv"))

# If correctly loaded, the datasets are displayed in the panel Environment
patient
hospital_stay
# Explore the dataset
##########################################################

str(patient)
head(patient)

# Explore hospital stay

str(hospital_stay)
?str
head(hospital_stay)

# Manipulate the dataset
##########################################################

# 1. Merge the two dataframes with the function merge
# the documentation is available with ?merge

patient_hospital_stay <- merge(patient,hospital_stay,by="patient_id")

# Explore the resulting dataset
patient_hospital_stay
# 2. Reformate date

# Display the documentation of the function strptime
?strptime
patient_hospital_stay$birth_date <-strptime( patient_hospital_stay$birth_date, format = "%Y%m%d")
patient_hospital_stay$admission_date <-strptime( patient_hospital_stay$admission_date, format = "%Y%m%d")
patient_hospital_stay$discharge_date <-strptime( patient_hospital_stay$discharge_date, format = "%Y%m%d")
patient_hospital_stay
# Transform the fields birth_date, admission_date, discharge_date
# to obtain a date
patient
# patient_stay$birth_date = #


# 3. Compute the difference between birth date and admission date
# with the function difftime
diff = difftime(patient_hospital_stay$admission_date,patient_hospital_stay$birth_date,units = "days")
diff/365
min(diff/365)
